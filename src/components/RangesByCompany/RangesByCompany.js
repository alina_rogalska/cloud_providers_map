import React, { useState, useEffect } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import axios from 'axios';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing(8)
  }
}));

const RangesByCompany = props => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [ipRanges, setIpRanges] = React.useState([]);

  const handleClick = () => {
    getIpRanges(props.companyId, props.locationId, props.filterDate);
    setOpen(!open);
  };

  const getIpRanges = async (companyId, locationId, filterDate) => {
    let result = await axios.get(`https://cloud-providers-map-api.herokuapp.com/ip_ranges?company_id=${companyId}&data_center_id=${locationId}&date=${moment(filterDate)}`)
      .catch(error => {
        console.log(error);
      });

    setIpRanges(result.data);
  }

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem button onClick={handleClick}>
        <ListItemText primary={props.companyName} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ul>
            {ipRanges.map((value, index) => {
              return <li><span>{`${value.network}`}</span> {`(${value.range})`}</li>
            })}
          </ul>
        </List>
      </Collapse>
    </List>
  );
}

export default RangesByCompany;
