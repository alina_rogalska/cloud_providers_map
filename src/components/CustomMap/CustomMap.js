import React from 'react';
import {Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import { Link } from 'react-router-dom';
import "./CustomMap.css";

class CustomMap extends React.Component {
  state={
    mapBoundaries: {}
  }

    render() {
      return (
      
        <div className="map-container">
        <LeafletMap
          center={[30, 10]}
          zoom={3}
          maxZoom={200}
          minZoom={2}
          attributionControl={true}
          zoomControl={true}
          doubleClickZoom={true}
          scrollWheelZoom={true}
          dragging={true}
          animate={true}
          easeLinearity={0.35}
        >
          <TileLayer
            url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          />
          {this.props.coordinates.map((value, index) => {
            return <Marker position={[value.latitude, value.longitude]}>
            <Popup>
              {value.city}<br></br>
              {value.network}<br></br>
              {value.companies.map((value, index) => {
                return <Link to={`/company/${value.id}`}>{(index > 0 ? ", " : "") + value.name}</Link>
               })}
              <br/>
              <Link to={'/location/' + value.id}>Details →</Link>
            </Popup>
            </Marker>;
          })};
          
        </LeafletMap>
        </div>
      );
    }
  }
  
  export default CustomMap;
