import React from 'react';
import logo from './logo.svg';
import './App.css';
import IpRadar from './containers/IpRadar/IpRadar';

import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <IpRadar></IpRadar>
      </div>
    </BrowserRouter>
  );
}

export default App;
