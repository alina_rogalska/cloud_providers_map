import React, { Component } from 'react';
import axios from 'axios';
import CustomMap from '../../../components/CustomMap/CustomMap';
import './CompanyDetails.css';
import moment from 'moment';

class CompanyDetails extends Component {
    state = {
        company: {},
        locationsCoordinates: []
    };

    componentDidMount() {
        this.getCompanyData();
        this.getCoordinates();
    }

    async getCompanyData() {
        const response = await axios.get('https://cloud-providers-map-api.herokuapp.com/companies/' + this.props.match.params.id)
            .catch(error => {
                console.log(error);
            });

        const company = response.data;
        this.setState({ company: company });
    }

    async getCoordinates() {
        const response =
            await axios.get(`https://cloud-providers-map-api.herokuapp.com/data_centers?company_id=${this.props.match.params.id}`);

        const locationsCoordinates = response.data;
        this.setState({ locationsCoordinates: locationsCoordinates });
    }

    render() {

        return (
            <div className="company-details">
                <h1 className="section-title">{this.state.company.name}</h1>
                <div className="details-list">
                    <div>
                        <span className="list-title">Website:</span>
                        <span className="list-value"><a href={this.state.company.website}>{this.state.company.website}</a></span>
                    </div>
                    <div>
                        <span className="list-title">Wikipedia link:</span>
                        <span className="list-value"><a href={this.state.company.wikipedia_link}>{this.state.company.wikipedia_link}</a></span>
                    </div>
                    <div>
                        <span className="list-title">Country:</span>
                        <span className="list-value">{this.state.company.country}</span>
                    </div>
                    <div>
                        <span className="list-title">Last update:</span>
                        <span className="list-value">{moment(this.state.company.updated_at).format("LLLL")}</span>
                    </div>
                    <p>{this.state.company.description}</p>
                </div>

                <CustomMap coordinates={this.state.locationsCoordinates}></CustomMap>

            </div>
        );
    };
}

export default CompanyDetails;