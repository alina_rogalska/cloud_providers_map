import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import IpLocationDetails from './IpLocationDetails/IpLocationDetails';
import CompanyDetails from './CompanyDetails/CompanyDetails';
import AllLocationsMap from './AllLocationsMap/AllLocationsMap';
import './IpRadar.css';

class IpRadar extends Component {
    render() {
        return (
            <div className="IpRadar">
                <header>
                    <a href="/" className="logo"><img src={process.env.PUBLIC_URL + "/map-icon.png"} ></img></a>
                    <span className="title">Providers' IP ranges location</span>
                </header>
                <Route path="/" exact component={AllLocationsMap} />
                <Route path="/location/:id" exact component={IpLocationDetails} />
                <Route path="/company/:id" exact component={CompanyDetails} />
            </div>
        );
    }
}

export default IpRadar;