import React, { Component } from 'react';
import axios from 'axios';
import CustomMap from '../../../components/CustomMap/CustomMap';
import './AllLocationsMap.css';

class AllLocationsMap extends Component {
  state = {
    coordinates: [],
    companies: []
  }

  componentDidMount() {
    this.getCoordinates();
    this.getCompanies();
  }

  async getCoordinates() {
    const response =
      await axios.get("https://cloud-providers-map-api.herokuapp.com/data_centers");

    const coordinates = response.data;
    this.setState({ coordinates: coordinates });
  }

  async getCoordinatesByCompany(id) {
    const response =
      await axios.get(`https://cloud-providers-map-api.herokuapp.com/data_centers?company_id=${id}`);

    const coordinates = response.data;
    this.setState({ coordinates: coordinates });
  }

  async getCompanies() {
    const response =
      await axios.get(`https://cloud-providers-map-api.herokuapp.com/companies/`);

    const companies = response.data;
    this.setState({ companies: companies });
  }

  onCompanyChanges(e) {
    if (e.target.value !== "all") {
      this.getCoordinatesByCompany(e.target.value);
    }
    else {
      this.getCoordinates();
    }
  }
  render() {

    return (
      <div className="all-locations-map">
        <div className="select-box">
          <select className="search-field" onChange={this.onCompanyChanges.bind(this)}>
            <option value="all">All Companies</option>
            {this.state.companies.map((value, key) => {
              return <option value={value.id}>{value.name}</option>
            })}
          </select>
        </div>
        <CustomMap coordinates={this.state.coordinates}></CustomMap>
      </div>
    );
  };
}

export default AllLocationsMap;