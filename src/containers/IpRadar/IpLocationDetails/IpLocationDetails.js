import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import axios from 'axios';
import "./IpLocationDetails.css";
import RangesByCompany from '../../../components/RangesByCompany/RangesByCompany';
import { withStyles } from '@material-ui/core/styles';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import moment from 'moment';

const styles = theme => ({
    root: {
        width: "100%",
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    }
});

class IpLocationDetalis extends Component {
    state = {
        ipLocation: {
            companies: []
        },
        ipRangesOpened: false,
        filterDate: moment(new Date()),
        dateMessage: "",
        ifEmpty: true
    }

    componentDidMount() {
        this.getLocation();
    }

    async getLocation(date) {

        const response = await axios.get('https://cloud-providers-map-api.herokuapp.com/data_centers/'
            + this.props.match.params.id + `?date=${moment((date ? date : new Date()))}`)
            .catch(error => {
                console.log(error);
            });

        const ipLocation = response.data.data_center;
        this.setState({ ipLocation: ipLocation });
        let dataUdated = moment(response.data.date).format("DD.MM.YYYY");

        if (typeof ipLocation.companies !== 'undefined' && ipLocation.companies.length > 0) {
            if (moment(response.data.date).format("YYYY-MM-DD") !== date) {
                this.setState({ dateMessage: `No data for given date. Data for the closest previous date: ${dataUdated}` });
            }
            else {
                this.setState({ dateMessage: `Data for: ${dataUdated}` });
            }

            this.setState({ ifEmpty: false });
        }
        else {
            this.setState({ dateMessage: `No data for given date.` });
            this.setState({ ifEmpty: true });
        }

        this.forceUpdate();
    }

    handleClick() {
        this.setState({ ipRangesOpened: !this.state.ipRangesOpened });
    }

    filterDateChanged(event) {
        this.setState({ ipRangesOpened: false });

        this.setState({ filterDate: event.target.value });
        this.getLocation(event.target.value);
    }

    render() {
        const { classes } = this.props;
        return (
            <div className="ip-radar">

                <h1 className="section-title">IP Location Details</h1>
                <div>
                    <span>Filter by date:</span>
                    <input type="date" className="date-field" onChange={this.filterDateChanged.bind(this)}
                        defaultValue={moment(new Date()).format("YYYY-MM-DD")}></input>
                    <br />
                    <p className="warning">{this.state.dateMessage}</p>
                </div>
                <div>
                    {(() => {
                        if (!this.state.ifEmpty) {
                            return (
                                <div>
                                    <div className="details-list">
                                        <div>
                                            <span className="list-title">Country:</span>
                                            <span className="list-value">{this.state.ipLocation.country}</span>
                                        </div>
                                        <div>
                                            <span className="list-title">City:</span>
                                            <span className="list-value">{this.state.ipLocation.city}</span>
                                        </div>
                                        <div>
                                            <span className="list-title">Latitude:</span>
                                            <span className="list-value">{this.state.ipLocation.latitude}</span>
                                        </div>
                                        <div>
                                            <span className="list-title">Longitude:</span>
                                            <span className="list-value">{this.state.ipLocation.longitude}</span>
                                        </div>
                                        <div>
                                            <span className="list-title">Last update:</span>
                                            <span className="list-value">{moment(this.state.ipLocation.updated_at).format("LLLL")}</span>
                                        </div>
                                        <div>
                                            <span className="list-title">Companies:</span>
                                            <span className="list-value">{
                                                this.state.ipLocation.companies.map((value, index) => {
                                                    return <Link to={`/company/${value.id}`}>{(index > 0 ? ", " : "") + value.name}</Link>
                                                })
                                            }</span>
                                        </div>
                                    </div >
                                    <List
                                        aria-labelledby="nested-list-subheader"
                                        className={classes.root}>
                                        <ListItem button onClick={this.handleClick.bind(this)}>
                                            <ListItemText primary="Show IP ranges" />
                                            {this.ipRangesOpened ? <ExpandLess /> : <ExpandMore />}
                                        </ListItem>
                                        <Collapse in={this.state.ipRangesOpened} timeout="auto" unmountOnExit>
                                            {this.state.ipLocation.companies.map((value, index) => {
                                                return <RangesByCompany locationId={this.props.match.params.id} companyId={value.id} companyName={value.name}
                                                    filterDate={this.state.filterDate} />;
                                            })}
                                        </Collapse>
                                    </List>
                                </div >
                            );
                        }
                    })()}
                </div>





            </div >
        );
    }
}

export default withStyles(styles)(IpLocationDetalis);